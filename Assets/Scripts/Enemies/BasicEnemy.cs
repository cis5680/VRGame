﻿
using System.Collections;
using UnityEngine;
using UnityEngine.VFX;

public class BasicEnemy : EnemyBase
{
    public override EnemyType EnemyType => EnemyType.Basic;

    [SerializeField] private Animator animator;
    [SerializeField] private SkinnedMeshRenderer meshRenderer;
    [SerializeField] private EnemyAnimationEventListener animationEventListener;
	[SerializeField] private AudioClip spawnSfx;
	[SerializeField] private AudioClip hitSfx;
	[SerializeField] private AudioClip die1Sfx;
	[SerializeField] private AudioClip die2Sfx;

    [SerializeField] private float danceTimeToDie = 10;
    private float timeDanceStarted;

	[Header("Death VFX")]
    [SerializeField] private float dieAnimDuration = 4.2f;
    [SerializeField] private float playDeathVFXAfterSeconds = 1.5f;
    [SerializeField] private AnimationCurve deathDissolveCurve;
    [SerializeField] private VisualEffect deathEffectGraph;

    protected override void Awake()
    {
        base.Awake();
        animationEventListener.OnCurrentAttackAnimationHit += DamagePlayer;
        animationEventListener.OnCurrentAttackAnimationDone += AttackAnimationCompleted;

        Spell_Dance.OnStartDancing += OnStartDancing;
        Spell_Dance.OnStopDancing += OnStopDancing;
	}

    protected override void Init()
    {
        deathEffectGraph.Stop();
        animator.Rebind();
        animator.Update(0f);

        foreach (Material mat in meshRenderer.materials)
        {
            mat.SetFloat("_DissolveAmount", 0.0f);
        }

        base.Init();
		AudioSource.PlayClipAtPoint(spawnSfx, transform.position);
		MoveToPlayer();
    }

    protected override void Update()
    {
        base.Update();

        animator.SetFloat("Speed", agent.velocity.sqrMagnitude);
    }

    private void OnStartDancing()
    {
        if (!IsAlive)
        {
            return;
        }

        animator.SetTrigger("Dance");
        animator.SetInteger("DanceIndex", Random.Range(0, 12));

        timeDanceStarted = Time.time;

        StopMoving();
        footstepsAudioSource.Stop();
        shouldTryToAttack = false;
        if (IsInvoking(nameof(AttackAnimationCompleted)))
        {
            CancelInvoke(nameof(AttackAnimationCompleted));
        }
    }

    private void OnStopDancing()
    {
        if (!IsAlive)
        {
            return;
        }

        animator.SetTrigger("StopDancing");

        shouldTryToAttack = true;
        float timeDanced = Time.time - timeDanceStarted;
        if (timeDanced > danceTimeToDie)
        {
            Die();
        }
        else
        {
            MoveToPlayer();
            footstepsAudioSource.Play();
        }
    }

    protected override void BeginAttackAnimation()
    {
        base.BeginAttackAnimation();

        animator.SetTrigger("Attack");
        animator.SetInteger("AttackIndex", Random.Range(0, 7));
		AudioSource.PlayClipAtPoint(hitSfx, transform.position);
	}

    protected override void BeginDeath()
    {
        if (Random.Range(0.0f, 1.0f) > 0.9f)
        {
            AudioSource.PlayClipAtPoint(die1Sfx, transform.position);
        }
        else
        {
            AudioSource.PlayClipAtPoint(die2Sfx, transform.position);
        }
		animator.SetTrigger("Die");
        StartCoroutine(DeathAnimation());
    }

    private IEnumerator DeathAnimation()
    {
        float timeSinceStarted = 0.0f;
        float percentageComplete = 0.0f;
        bool playedEffect = false;

        while (percentageComplete < 1.0f)
        {
            timeSinceStarted += Time.deltaTime;
            percentageComplete = timeSinceStarted / dieAnimDuration;

            if (timeSinceStarted > playDeathVFXAfterSeconds && !playedEffect)
            {
                playedEffect = true;
                deathEffectGraph.Play();
            }

            foreach (Material mat in meshRenderer.materials)
            {
                mat.SetFloat("_DissolveAmount", deathDissolveCurve.Evaluate(percentageComplete));
            }

            yield return null;
        }

        FinishDeath();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        animationEventListener.OnCurrentAttackAnimationHit -= DamagePlayer;
        animationEventListener.OnCurrentAttackAnimationDone -= AttackAnimationCompleted;

        Spell_Dance.OnStartDancing -= OnStartDancing;
        Spell_Dance.OnStopDancing -= OnStopDancing;
    }
}
