using System;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent), typeof(AudioSource))]
public abstract class EnemyBase : MonoBehaviour, IDamageable
{
    public abstract EnemyType EnemyType { get; }

    [SerializeField] protected float maxHealth;
    protected float curHealth;
    public float damage;
    public bool IsAlive { get; private set; } = false;

    protected NavMeshAgent agent;
    protected bool canHitPlayer { get; private set; }
    private float timeTillNextAttack = 0.0f;
    [SerializeField] private float regularAttackInterval = 3.0f;
    protected bool isPlayingAttackAnimation = false;
    protected bool shouldTryToAttack = true;

	protected AudioSource footstepsAudioSource;

	protected virtual void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        footstepsAudioSource = GetComponent<AudioSource>();
    }

    protected virtual void Init()
    {
        IsAlive = true;
        curHealth = maxHealth;
	}

    public void Spawn(Vector3 position, Quaternion rotation)
    {
        transform.position = position;
        transform.rotation = rotation;
        gameObject.SetActive(true);
        Init();
    }

    public virtual void TakeDamage(float damage)
    {
        curHealth -= damage;
        if (curHealth <= 0)
        {
            curHealth = 0;
            Die();
        }
    }

    public static event Action<EnemyBase> OnKilled;
    public void Die()
    {
        IsAlive = false;
        GameManager.Instance.updateScore(1);

        BeginDeath();   // begin death MUST fire FinishDeath()!!!
    }

    protected abstract void BeginDeath();
    
    protected virtual void FinishDeath()
    {
        gameObject.SetActive(false);
        OnKilled?.Invoke(this);
    }

    protected virtual void MoveToPlayer()
    {
        agent.isStopped = false;
        agent.SetDestination(LevelManager.Instance.Player.transform.position);		
		footstepsAudioSource.Play();
	}

    protected virtual void StopMoving()
    {
        agent.isStopped = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerWeapon"))
        {
            IPlayerWeapon weapon = other.GetComponent<IPlayerWeapon>();
            if (weapon.CanDamageEnemy())
            {
                weapon.OnEnemyHit(this);
                TakeDamage(weapon.DamageToEnemy);
            }
        }
    }

    protected virtual void Update()
    {
        if (!shouldTryToAttack)
        {
            return;
        }

        if (agent.remainingDistance <= agent.stoppingDistance)
        {
            if (!isPlayingAttackAnimation)
            {
                timeTillNextAttack -= Time.deltaTime;
                if (timeTillNextAttack <= 0)
                {
                    canHitPlayer = true;
					footstepsAudioSource.Stop();
					BeginAttackAnimation();
                }
            }
        }
        else
        {
            canHitPlayer = false;
        }
    }

    protected virtual void BeginAttackAnimation()
    {
        isPlayingAttackAnimation = true;
        Invoke(nameof(AttackAnimationCompleted), 3.0f); // force!
    }

    protected virtual void AttackAnimationCompleted()
    {
        if (IsInvoking(nameof(AttackAnimationCompleted)))
        {
            CancelInvoke(nameof(AttackAnimationCompleted));
        }

        timeTillNextAttack = regularAttackInterval;
        isPlayingAttackAnimation = false;
        canHitPlayer = false;
    }

    protected void DamagePlayer()
    {
        if (canHitPlayer)
        {
            GameManager.Instance.updateHealth(damage);
            GameManager.Instance.checkWinLoss();
        }
    }

    protected virtual void OnDestroy()
    {

    }
}

public enum EnemyType
{
    None = -1,
    Basic
}