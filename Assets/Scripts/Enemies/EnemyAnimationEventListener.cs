using System;
using UnityEngine;

public class EnemyAnimationEventListener : MonoBehaviour
{
    public event Action OnCurrentAttackAnimationHit;
    public void RegisterAttackHit()
    {
        OnCurrentAttackAnimationHit?.Invoke();
    }

    public event Action OnCurrentAttackAnimationDone;
    public void AttackAnimationCompleted()
    {
        OnCurrentAttackAnimationDone?.Invoke();
    }
}
