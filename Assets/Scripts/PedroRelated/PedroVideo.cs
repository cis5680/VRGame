using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Video;

[RequireComponent(typeof(VideoPlayer))]
public class PedroVideo : MonoBehaviour
{
    private VideoPlayer vp;
    [SerializeField] private float volumeCrossfadeDuration = 0.3f;
    [SerializeField] private float maxVol = 0.5f;
    [SerializeField] private AudioSource pedroAudioSource;
    private Coroutine volumeCoroutine = null;

    private void Awake()
    {
        vp = GetComponent<VideoPlayer>();
        Spell_Dance.OnStartDancing += PlayVideo;
        Spell_Dance.OnStopDancing += StopVideo;
    }

    private void PlayVideo()
    {
        vp.Play();
        vp.SetDirectAudioVolume(0, 0.0f);
        pedroAudioSource.Play();
        pedroAudioSource.volume = 0.0f;
        SetVolume(maxVol);
    }

    private void StopVideo()
    {
        SetVolume(0.0f, () =>
        {
            vp.Stop();
            pedroAudioSource.Stop();
        });
    }

    private void SetVolume(float volume, Action onDone = null)
    {
        if (volumeCoroutine != null)
        {
            StopCoroutine(volumeCoroutine);
        }
        volumeCoroutine = StartCoroutine(VolumeCoroutine(volume, onDone));
    }

    private IEnumerator VolumeCoroutine(float targetVolume, Action onDone = null)
    {
        float timeSinceStarted = 0.0f;
        float percentageCompleted = 0.0f;

        float startVol = vp.GetDirectAudioVolume(0);

        while (timeSinceStarted <= volumeCrossfadeDuration)
        {
            timeSinceStarted += Time.deltaTime;
            percentageCompleted = timeSinceStarted / volumeCrossfadeDuration;

            float vol = Mathf.Lerp(startVol, targetVolume, percentageCompleted);
            vp.SetDirectAudioVolume(0, vol);
            pedroAudioSource.volume = vol;
            yield return null;
        }

        vp.SetDirectAudioVolume(0, targetVolume);
        pedroAudioSource.volume = targetVolume;
        onDone?.Invoke();
    }

    private void OnDestroy()
    {
        Spell_Dance.OnStartDancing -= PlayVideo;
        Spell_Dance.OnStopDancing -= StopVideo;
    }
}
