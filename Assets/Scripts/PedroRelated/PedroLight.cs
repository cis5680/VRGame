using System;
using System.Collections;
using UnityEngine;

public class PedroLight : MonoBehaviour
{
    [SerializeField] private Light light;
    private Quaternion normalRot;
    [SerializeField] private Transform pedroLightTransform;
    [SerializeField] private float animOnDuration = 1.2f;
    [SerializeField] private float animOffDuration = 0.3f;
    private float moonNormalSize;
    [SerializeField] private float moonPedroSize = 0.5f;
    private Vector3 moonMaskNormalOffset;
    [SerializeField] private Vector3 moonMaskPedroOffset;
    [SerializeField] private Material pedroSkyboxMaterial;
    [SerializeField] private Gradient pedroHorizonColorGradient;
    private Color horizonNormalColor;
    [SerializeField] private AnimationCurve animOnCurve, animOffCurve;
    private Coroutine moonCoroutine = null;

    private void Awake()
    {
        normalRot = transform.rotation;
        Spell_Dance.OnStartDancing += OnPedroIsHere;
        Spell_Dance.OnStopDancing += OnPedroIsGoingButHeWillReturnVerySoonBecauseHeIsLoved;
    }

    private void Start()
    {
        moonNormalSize = pedroSkyboxMaterial.GetFloat("_MoonSize");
        moonMaskNormalOffset = pedroSkyboxMaterial.GetVector("_MoonMaskOffset");
        horizonNormalColor = pedroSkyboxMaterial.GetColor("_HorizonColor");
    }

    private void OnPedroIsHere()
    {
        SetMoon(true);
    }

    private void OnPedroIsGoingButHeWillReturnVerySoonBecauseHeIsLoved()
    {
        SetMoon(false);
    }

    private void SetMoon(bool on, Action onDone = null)
    {
        if (moonCoroutine != null)
        {
            StopCoroutine(moonCoroutine);
        }
        moonCoroutine = StartCoroutine(MoonCoroutine(on, onDone));
    }

    private IEnumerator MoonCoroutine(bool on, Action onDone = null)
    {
        float timeSinceStarted = 0.0f;
        float percentageCompleted = 0.0f;

        Quaternion startRot = transform.rotation;
        Quaternion endRot = on ? pedroLightTransform.rotation : normalRot;

        float startMoonSize = pedroSkyboxMaterial.GetFloat("_MoonSize"); 
        float endMoonSize = on ? moonPedroSize : moonNormalSize;

        Vector3 startMaskOffset = pedroSkyboxMaterial.GetVector("_MoonMaskOffset");
        Vector3 endMaskOffset = on ? moonMaskPedroOffset : moonMaskNormalOffset;

        AnimationCurve curve = on ? animOnCurve : animOffCurve;

        float startPedroAmount = pedroSkyboxMaterial.GetFloat("_PedroAmount");
        float endPedroAmount = on ? 1.0f : 0.0f;

        Gradient gradient = pedroHorizonColorGradient;
        Color startColor = pedroSkyboxMaterial.GetColor("_HorizonColor");
        
        float duration = on ? animOnDuration : animOffDuration;

        while (timeSinceStarted <= duration)
        {
            timeSinceStarted += Time.deltaTime;
            percentageCompleted = timeSinceStarted / duration;
            float t = curve.Evaluate(percentageCompleted);

            transform.rotation = Quaternion.SlerpUnclamped(startRot, endRot, t);
            pedroSkyboxMaterial.SetFloat("_MoonSize", Mathf.LerpUnclamped(startMoonSize, endMoonSize, t));
            pedroSkyboxMaterial.SetFloat("_PedroAmount", Mathf.Lerp(startPedroAmount, endPedroAmount, percentageCompleted));
            pedroSkyboxMaterial.SetVector("_MoonMaskOffset", Vector3.LerpUnclamped(startMaskOffset, endMaskOffset, t));
            pedroSkyboxMaterial.SetColor("_HorizonColor", on? gradient.Evaluate(percentageCompleted) : Color.Lerp(startColor, horizonNormalColor, percentageCompleted));
            
            yield return null;
        }


        transform.rotation = endRot;
        pedroSkyboxMaterial.SetFloat("_MoonSize", endMoonSize);
        pedroSkyboxMaterial.SetFloat("_PedroAmount", endPedroAmount);
        pedroSkyboxMaterial.SetVector("_MoonMaskOffset", endMaskOffset);
        pedroSkyboxMaterial.SetColor("_HorizonColor", on ? gradient.Evaluate(1.0f) : horizonNormalColor);

        onDone?.Invoke();
    }

    private void OnDestroy()
    {
        // Reset material properties
        pedroSkyboxMaterial.SetFloat("_MoonSize", moonNormalSize); 
        pedroSkyboxMaterial.SetFloat("_PedroAmount", 0.0f);
        pedroSkyboxMaterial.SetVector("_MoonMaskOffset", moonMaskNormalOffset);
        pedroSkyboxMaterial.SetColor("_HorizonColor", horizonNormalColor);

        Spell_Dance.OnStartDancing -= OnPedroIsHere;
        Spell_Dance.OnStopDancing -= OnPedroIsGoingButHeWillReturnVerySoonBecauseHeIsLoved;
    }
}
