using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.Inputs;
using Valve.VR.InteractionSystem;

public class PedroTimeControl : MonoBehaviour
{
    [SerializeField] private VelocityEstimator head;
    [SerializeField] private VelocityEstimator leftHand;
    [SerializeField] private VelocityEstimator rightHand;

    [SerializeField] private float minTimeScale;
    [SerializeField] private AnimationCurve timeScaleCurve;

    [SerializeField] private AudioSource pedroAudioSource;
    [SerializeField] private float slowMoPitch;
    private float normalPitch;

    private bool isDancing = false;

    private void Awake()
    {
        Spell_Dance.OnStartDancing += OnStartDancing;
        Spell_Dance.OnStopDancing += OnStopDancing;
    }

    private void Start()
    {
        normalPitch = pedroAudioSource.pitch;
    }

    private void OnStartDancing()
    {
        head.BeginEstimatingVelocity();
        leftHand.BeginEstimatingVelocity();
        rightHand.BeginEstimatingVelocity();
        isDancing = true;
    }

    private void OnStopDancing()
    {
        head.FinishEstimatingVelocity();
        leftHand.FinishEstimatingVelocity();
        rightHand.FinishEstimatingVelocity();
        isDancing = false;
        Time.timeScale = 1.0f;
        Time.fixedDeltaTime = 0.02f;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDancing)
        {
            float velMag = head.GetVelocityEstimate().sqrMagnitude
                + leftHand.GetVelocityEstimate().sqrMagnitude
                + rightHand.GetVelocityEstimate().sqrMagnitude;
                //+ head.GetAngularVelocityEstimate().sqrMagnitude
                //+ leftHand.GetAngularVelocityEstimate().sqrMagnitude
                //+ rightHand.GetAngularVelocityEstimate().sqrMagnitude;

            float t = timeScaleCurve.Evaluate(velMag);
            Time.timeScale = Mathf.Clamp(t, minTimeScale, 1.0f);
            Time.fixedDeltaTime = 0.02f * Time.timeScale;

            pedroAudioSource.pitch = Mathf.Lerp(slowMoPitch, normalPitch, t);
            pedroAudioSource.volume = Mathf.Lerp(0.05f, 0.5f, t);
        }
    }

    private void OnDestroy()
    {
        Spell_Dance.OnStartDancing -= OnStartDancing;
        Spell_Dance.OnStopDancing -= OnStopDancing;
    }
}
