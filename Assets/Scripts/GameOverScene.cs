using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


public class GameOverScene : MonoBehaviour
{
    GameObject exitButton;
    GameObject restartButton;
    GameObject scoreLabel;
    GameObject highScoreLabel;

    void Start()
    {
        exitButton = GameObject.Find("Exit Button");
        restartButton = GameObject.Find("Restart Button");
        scoreLabel = GameObject.Find("ScoreLabel");
        highScoreLabel = GameObject.Find("HighScoreLabel");

        // Update Score
        TextMeshProUGUI scoreText = scoreLabel.GetComponent<TextMeshProUGUI>();
        scoreText.text = "Score: " + GameManager.Instance.getScore().ToString();

        // Check High Score
        if (GameManager.Instance.checkHighScore())
        {
            TextMeshProUGUI highScoreText = highScoreLabel.GetComponent<TextMeshProUGUI>();
            highScoreText.text = "NEW HIGH SCORE!";
        }
    }

    public void OnRestartClicked()
    {
        GameManager.Instance.resetGame();
        SceneManager.LoadScene("BasicScene");
    }

    public void OnExitClicked()
    {
        Application.Quit();
    }
}
