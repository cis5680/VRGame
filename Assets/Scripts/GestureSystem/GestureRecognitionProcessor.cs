using System;
using UnityEngine;

public enum GestureName
{
    None = -1,
    SymbolO,
	SymbolZ,
    SymbolThrow, 
    SymbolInf, 
    SymbolLasso,
    SymbolUnusedDONTUSEME = 999
};

public class GestureRecognitionProcessor : MonoBehaviour
{
	//[SerializeField] protected SpellMagicMissile magicMissileSpell;
    [Range(0.0f, 1.0f)]
    public float similarityThreshold;

	//private Dictionary<GestureName, SpellBase> gestureLookupDict;

    void Start()
    {
		//gestureLookupDict = new Dictionary<GestureName, SpellBase>();
		//gestureLookupDict.Add(GestureName.SymbolZ, magicMissileSpell);
  //      gestureLookupDict.Add(GestureName.SymbolO, magicMissileSpell);
    }

    public static event Action<GestureName> OnGestureCompletedEvent;

    public void OnGestureCompleted(GestureCompletionData gestureCompletionData)
    {
        if (gestureCompletionData.gestureID < 0)
        {
            //TODO: idk how we really want to handle the case when the recognition itself failed?
            string errorMessage = GestureRecognition.getErrorMessage(gestureCompletionData.gestureID);
            Debug.Log("error bro: " + errorMessage);
            return;
        }
        if (gestureCompletionData.similarity >= similarityThreshold)
        {
			GestureName recognizedGesture = (GestureName)Enum.Parse(typeof(GestureName), gestureCompletionData.gestureName);
			Debug.Log("Registered! Spell Name: " + gestureCompletionData.gestureName + " , ID: " + gestureCompletionData.gestureID);
            //gestureLookupDict[recongizedGesture].CastSpell();
            OnGestureCompletedEvent?.Invoke(recognizedGesture);
		}
        else
		{
			//TODO: no spell was recognized, probably play some sound and/or have spell turning red and fading away? Whatever, the logic for that will go here
            Debug.Log("Oopsie Daisy");
		}
	}

    [ContextMenu("Test Symbol Z")]
    private void TestSymbolZ()
    {
        OnGestureCompletedEvent?.Invoke(GestureName.SymbolZ);
    }

    [ContextMenu("Test Symbol O")]
    private void TestSymbolO()
    {
        OnGestureCompletedEvent?.Invoke(GestureName.SymbolO);
    }
}