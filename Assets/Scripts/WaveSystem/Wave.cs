﻿using System;
using UnityEngine;

public abstract class Wave : MonoBehaviour
{
    public bool IsActive { get; protected set; } = false;
    public abstract WaveType WaveType { get; }

    protected WaveData myWaveData;

    public event Action OnStarted;
    public event Action OnEnded;

    public virtual void SetupWave(WaveData myWaveData)
    {
        this.myWaveData = myWaveData;
    }

    public virtual void StartWave()
    {
        IsActive = true;
        OnStarted?.Invoke();
    }

    public virtual void EndWave()
    {
        IsActive = false;
        OnEnded?.Invoke();
    }
}