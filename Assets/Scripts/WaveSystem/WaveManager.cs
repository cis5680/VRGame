using UnityEngine;

public class WaveManager : MonoBehaviour
{
    [SerializeField] private WaveData[] waves;

    private WaitWave waitWave;
    private HordeSurvivalWave hordeWave;

    private WaveData currActiveWaveData;
    private Wave currActiveWave;
    private int currWaveIdx = 0;

    private void Awake()
    {
        waitWave = gameObject.AddComponent<WaitWave>();
        hordeWave = gameObject.AddComponent<HordeSurvivalWave>();
    }

    [ContextMenu("Start Wave")]
    public void StartWaves()
    {
        currWaveIdx = -1;
        StartNextWave();
    }

    private void StartNextWave()
    {
        currWaveIdx++;
        currActiveWaveData = waves[currWaveIdx];

        currActiveWave = currActiveWaveData.WaveType switch
        {
            WaveType.Wait => waitWave,
            WaveType.HordeSurvival => hordeWave,
            _ => waitWave
        };

        currActiveWave.OnEnded += OnCurrentWaveEnded;
        currActiveWave.SetupWave(currActiveWaveData);
        currActiveWave.StartWave();
    }

    private void OnCurrentWaveEnded()
    {
        currActiveWave.OnEnded -= OnCurrentWaveEnded;
        StartNextWave();
    }

    private void OnDestroy()
    {
        if (currActiveWave != null)
        {
            currActiveWave.OnEnded -= OnCurrentWaveEnded;
        }
    }
}