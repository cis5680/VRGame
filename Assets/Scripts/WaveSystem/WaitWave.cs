﻿public class WaitWave : Wave
{
    public override WaveType WaveType => WaveType.Wait;

    private WaitWaveData waitWaveData;
    public override void SetupWave(WaveData myWaveData)
    {
        base.SetupWave(myWaveData);
        waitWaveData = (WaitWaveData)myWaveData;
    }

    public override void StartWave()
    {
        if (IsInvoking(nameof(EndWave)))
        {
            CancelInvoke(nameof(EndWave));
        }
        Invoke(nameof(EndWave), waitWaveData.WaitTime);

        base.StartWave();
    }
}
