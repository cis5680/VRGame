﻿using System.Collections.Generic;
using UnityEngine;

public class HordeSurvivalWave : Wave
{
    public override WaveType WaveType => WaveType.HordeSurvival;

    private HordeSurvivalWaveData currentHordeDetails;
    public Dictionary<EnemyType, float> EnemySpawnChancesDict { get; private set; } = new Dictionary<EnemyType, float>();

    private float totalSpawnChance;

    private int totalKilledEnemies;
    private int totalSpawnedEnemies;

    public override void SetupWave(WaveData myWaveData)
    {
        base.SetupWave(myWaveData);
        currentHordeDetails = (HordeSurvivalWaveData)myWaveData;

        EnemySpawnChancesDict.Clear();
        totalSpawnChance = 0.0f;
        totalKilledEnemies = 0;
        totalSpawnedEnemies = 0;

        for (int i = 0; i < currentHordeDetails.EnemyDetails.Length; i++)
        {
            HordeSurvivalWaveData.EnemySpawnDetails details = currentHordeDetails.EnemyDetails[i];
            EnemySpawnChancesDict.Add(details.EnemyType, details.SpawnChance);
            totalSpawnChance += details.SpawnChance;
        }
    }

    public override void StartWave()
    {
        EnemyBase.OnKilled += OnEnemyKilled;

        SpawnNextSubwave();

        base.StartWave();
    }

    private void SpawnNextSubwave()
    {
        int enemiesLeftToSpawn = currentHordeDetails.NumEnemies - totalSpawnedEnemies;
        int numEnemiesToSpawn = Mathf.Min(enemiesLeftToSpawn, currentHordeDetails.SubWaveSpawnCount);

        for (int i = 0; i < numEnemiesToSpawn; i++)
        {
            float rng = Random.Range(0.0f, totalSpawnChance);

            EnemyType enemyToSpawn = EnemyType.None;

            foreach (KeyValuePair<EnemyType, float> enemyType in EnemySpawnChancesDict)
            {
                if (enemyType.Value > rng)
                {
                    enemyToSpawn = enemyType.Key;
                    break;
                }
            }

            // Spawn enemy type
            //Debug.Log("Spawning " + enemyToSpawn);
            if (enemyToSpawn != EnemyType.None)
            {
                EnemiesManager.Instance.SpawnEnemy(enemyToSpawn);
                totalSpawnedEnemies++;
            }
        }

        if (totalSpawnedEnemies < currentHordeDetails.NumEnemies)
        {
            // Enemies left in wave, spawn after delay
            if (IsInvoking(nameof(SpawnNextSubwave)))
            {
                CancelInvoke(nameof(SpawnNextSubwave));
            }

            Invoke(nameof(SpawnNextSubwave), currentHordeDetails.WaitBetweenSubWaves);
        }
    }

    private void OnEnemyKilled(EnemyBase enemy)
    {
        totalKilledEnemies++;
        if (totalKilledEnemies >= currentHordeDetails.NumEnemies)
        {
            EndWave();
        }
    }

    public override void EndWave()
    {
        base.EndWave();
        EnemyBase.OnKilled -= OnEnemyKilled;
    }
}
