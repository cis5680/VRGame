using System;
using UnityEngine;

public class Player : MonoBehaviour, IDamageable, IHealable
{
    [SerializeField] private float maxHealth = 100;


    private float curHealth;
    public bool IsAlive { get; private set; } = true;

    private void Awake()
    {
        ResetVariables();
    }

	private void Start() {
        //spellbook = GameObject.FindObjectOfType<SpellbookInterface>();
	}

	private void Update() {
        // THIS IS VERY BAD TEMP STUFF FOR SPELL CASTING
		if (Input.GetKeyDown(KeyCode.Alpha1)) {
			OnGestureCompletedEvent?.Invoke(GestureName.SymbolO);
        }
		if (Input.GetKeyDown(KeyCode.Alpha2)) {
			OnGestureCompletedEvent?.Invoke(GestureName.SymbolZ);
		}
		if (Input.GetKeyDown(KeyCode.Alpha3)) {
			OnGestureCompletedEvent?.Invoke(GestureName.SymbolThrow);
		}
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnGestureCompletedEvent?.Invoke(GestureName.SymbolUnusedDONTUSEME);
        }
        if (Input.GetButtonDown("XRI_Right_PrimaryButton"))
        {
            OnGestureCompletedEvent?.Invoke(GestureName.SymbolUnusedDONTUSEME);
        }
    }

	public static event Action<GestureName> OnGestureCompletedEvent;

	private void ResetVariables()
    {
        curHealth = maxHealth;
        IsAlive = true;
    }

    public void Heal(float healAmount)
    {
        if (!IsAlive)
        {
            return;
        }

        curHealth = Mathf.Clamp(curHealth + healAmount, 0.0f, maxHealth);
    }

    public void TakeDamage(float damage)
    {
        if (!IsAlive)
        {
            return;
        }

        curHealth -= damage;
        //spellbook.UpdateHealthUI(curHealth / maxHealth);

		if (curHealth <= 0.0f)
        {
            Die();
            curHealth = 0.0f;
        }
    }

    public static event Action OnKilled;
    private void Die()
    {
        IsAlive = false;
        OnKilled?.Invoke();
    }
}
