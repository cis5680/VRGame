using UnityEngine;

[ExecuteInEditMode]
public class SetSunDirectionGlobalShader : MonoBehaviour
{
    // Start is called before the first frame update
    void Update()
    {
        Shader.SetGlobalVector("_SunDirection", transform.forward);
    }
}
