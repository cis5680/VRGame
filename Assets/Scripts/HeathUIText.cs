using System.Collections;
using System.Collections.Generic;
using static System.Math;
using UnityEngine;
using UnityEngine.UI;

public class HeathUIText : MonoBehaviour
{
    Text healthText;

    // Start is called before the first frame update
    void Start()
    {
        healthText = gameObject.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        float h = GameManager.Instance.getHealth();
        healthText.text = ((int)System.Math.Round(h)).ToString();
    }
}
