using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SpellbookInterface : MonoBehaviour
{
	// ------------------------ REFERENCES ----------------------------
	// stuff for page FX
	[SerializeField] private Material pageMaterial;
	[SerializeField] private List<Texture2D> spellGlyphs;
	[SerializeField] private Texture2D nullGlyph;
	[SerializeField] private MeshRenderer bookRenderer;

	// stuff for cooldowns
	[SerializeField] private List<CooldownTab> cooldownTabs;

	[SerializeField] private Image healthBarFill;

	[SerializeField] private List<SpellBase> knownSpells = new List<SpellBase>();
	private Dictionary<GestureName, SpellBase> spellsDict;

	private List<SpellBase> spellsOnCooldown = new List<SpellBase>();

	private Material mat_leftPage, mat_rightPage;

	private int leftPageIndex = 0;
	private int totalSpells = 0;


	public static SpellbookInterface Instance { get; private set; }

	[SerializeField] private Transform projectileSpawnLocation;

    private void Awake()
    {
        if (Instance == null)
        {
			Instance = this;
        }
		else if (Instance != this)
        {
			Destroy(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
		spellsDict = new Dictionary<GestureName, SpellBase>();

		foreach (SpellBase spell in knownSpells) {
			SpellBase instantiatedSpell = Instantiate(spell);
			spellsDict.Add(instantiatedSpell.spellData.inputIdentifier, instantiatedSpell);
			totalSpells++;

			if (instantiatedSpell is SpellMagicMissile)
			{
				(instantiatedSpell as SpellMagicMissile).missileSpawnLocation = projectileSpawnLocation;
			}
			else if (instantiatedSpell is Spell_Sword)
			{
				(instantiatedSpell as Spell_Sword).swordSpawnLocation = projectileSpawnLocation;
			}
			else if (instantiatedSpell is Spell_Fireball)
			{
				(instantiatedSpell as Spell_Fireball).fireballSpawnLocation = projectileSpawnLocation;
			}
		}

		mat_leftPage = bookRenderer.materials[1];
		mat_rightPage = bookRenderer.materials[0];

		UpdateMaterials();

		foreach (CooldownTab tab in cooldownTabs) {
			tab.Deactivate();
		}

		Player.OnGestureCompletedEvent += (spellInput) => TryUseSpell(spellInput);
		GestureRecognitionProcessor.OnGestureCompletedEvent += (spellInput) => TryUseSpell(spellInput);
	}

	private void OnDestroy() {
		Player.OnGestureCompletedEvent -= (spellInput) => TryUseSpell(spellInput);
		GestureRecognitionProcessor.OnGestureCompletedEvent -= (spellInput) => TryUseSpell(spellInput);
	}

	// Update is called once per frame
	void Update()
    {
		if (Input.GetKeyDown(KeyCode.E)) {
			TurnPageForward();
		}
		if (Input.GetKeyDown(KeyCode.Q)) {
			TurnPageBackward();
		}

		for (int i = spellsOnCooldown.Count - 1; i >= 0; i--) {
			SpellBase spell = spellsOnCooldown[i];
			cooldownTabs[i].SetFill(1f - (spell.cooldownTimeRemaining / spell.spellData.cooldownDuration));
		}

		// clear any tabs that are for spells that are no longer on cooldown
		for (int i = spellsOnCooldown.Count - 1; i >= 0; i--) {
			if (!spellsOnCooldown[i].isOnCooldown) {
				spellsOnCooldown.RemoveAt(i);
				for (int j = i; j < cooldownTabs.Count; j++) {
					if (j < cooldownTabs.Count - 1 && j < spellsOnCooldown.Count) {
						cooldownTabs[j].Copy(cooldownTabs[j + 1]);
					}
					else {
						cooldownTabs[j].Deactivate();
					}
				}
				cooldownTabs[spellsOnCooldown.Count].Deactivate();
			}
		}
	}

	public void TurnPageForward() {
		if (leftPageIndex + 2 < totalSpells) {
			leftPageIndex += 2;
			UpdateMaterials();
		}
	}

	public void TurnPageBackward() {
		if (leftPageIndex - 2 >= 0) {
			leftPageIndex -= 2;
			UpdateMaterials();
		}
	}

	private void UpdateMaterials() {
		// TODO:
		// Dict.Values.ElementAt is non-performant we should change this later

		if (totalSpells > leftPageIndex) {
			mat_leftPage.SetTexture("_GlyphTexture", spellsDict.Values.ElementAt(leftPageIndex).spellData.glyph);
		}
		else {
			mat_leftPage.SetTexture("_GlyphTexture", nullGlyph);
		}

		if (totalSpells > leftPageIndex + 1) {
			mat_rightPage.SetTexture("_GlyphTexture", spellsDict.Values.ElementAt(leftPageIndex + 1).spellData.glyph);
		}
		else {
			mat_rightPage.SetTexture("_GlyphTexture", nullGlyph);
		}
	}

	private void TryUseSpell(GestureName inputIdentifier) {
		if (spellsDict.ContainsKey(inputIdentifier))
        {
			SpellBase spell = spellsDict[inputIdentifier];
			if (spell.TryCastSpell())
            {
				PutSpellOnCooldown(spell);
			}
		}
	}

	public void PutSpellOnCooldown(SpellBase _usedSpell) {
		spellsOnCooldown.Add(_usedSpell);
		cooldownTabs[spellsOnCooldown.Count - 1].SetFill(0f);
		cooldownTabs[spellsOnCooldown.Count - 1].SetIcon(_usedSpell.spellData.icon);
		cooldownTabs[spellsOnCooldown.Count - 1].SetColor(_usedSpell.spellData.color);
		cooldownTabs[spellsOnCooldown.Count - 1].Activate();
	}

	public void UpdateHealthUI(float _fill) {
		healthBarFill.fillAmount = _fill;
	}
}
