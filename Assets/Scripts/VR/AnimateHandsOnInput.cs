using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Animator))]
public class AnimateHandsOnInput : MonoBehaviour
{
    [SerializeField] private InputActionProperty pinchAction;
    [SerializeField] private InputActionProperty gripAction;

    private Animator animator;

    // Start is called before the first frame update
    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float triggerVal = pinchAction.action.ReadValue<float>();
        animator.SetFloat("Trigger", triggerVal);

        float gripVal = gripAction.action.ReadValue<float>();
        animator.SetFloat("Grip", gripVal);
    }
}
