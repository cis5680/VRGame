using System.Collections.Generic;
using UnityEngine;
using Utkarsh.UnityCore.Pooling;

public class EnemiesManager : MonoBehaviour
{
    public static EnemiesManager Instance { get; private set; }

    [SerializeField] private EnemyData[] enemies;
    private Dictionary<EnemyType, EnemyData> enemiesDict;

    private Dictionary<EnemyType, DynamicPool<EnemyBase>> enemyPools;

    [SerializeField] private Transform center;
    [SerializeField] private float rangeInner = 30.0f;
    [SerializeField] private float rangeOuter = 50.0f;

    [SerializeField] private int initialEnemyPoolSize = 20;
    private Transform enemiesHolder;

    private void Awake()
    {
        // Singleton
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }

        SetupEnemyPools();
        EnemyBase.OnKilled += OnEnemyKilled;
    }

    private void SetupEnemyPools()
    {
        enemiesHolder = new GameObject("EnemiesHolder").transform;

        enemiesDict = new Dictionary<EnemyType, EnemyData>();
        enemyPools = new Dictionary<EnemyType, DynamicPool<EnemyBase>>();

        foreach (EnemyData data in enemies)
        {
            enemiesDict.Add(data.EnemyType, data);
            enemyPools.Add(data.EnemyType, new DynamicPool<EnemyBase>(() =>
            {
                EnemyBase enemy = Instantiate(data.EnemyPrefab, enemiesHolder);
                enemy.gameObject.SetActive(false);
                return enemy;
            }, initialEnemyPoolSize));
        }
    }

    public void SpawnEnemy(EnemyType enemyType)
    {
        Vector3 enemySpawnPos = GetEnemySpawnPoint();
        Quaternion rot = Quaternion.LookRotation((LevelManager.Instance.Player.transform.position - enemySpawnPos).normalized, Vector3.up);
        EnemyBase enemy = enemyPools[enemyType].GetPooledObject();
        enemy.Spawn(enemySpawnPos, rot);
    }

    private Vector3 GetEnemySpawnPoint()
    {
        int iters = 30;

        float dist = Random.Range(rangeInner, rangeOuter);
        Vector3 dir;
        Vector3 pt = Vector3.zero;

        for (int i = 0; i < iters; i++)
        {
            dir = Quaternion.Euler(0.0f, Random.Range(0.0f, 360.0f), 0.0f) * Vector3.forward;
            Vector3 randomPoint = center.position + dir * dist;
            UnityEngine.AI.NavMeshHit hit;
            if (UnityEngine.AI.NavMesh.SamplePosition(randomPoint, out hit, 1.0f, UnityEngine.AI.NavMesh.AllAreas))
            {
                pt = hit.position;
                return pt;
            }
        }
        pt.y = 1.0f;
        return pt;
    }

    private void OnEnemyKilled(EnemyBase enemy)
    {
        enemyPools[enemy.EnemyType].ReturnToPool(enemy);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(center.position, center.position + Vector3.forward * rangeOuter);
        Gizmos.DrawWireSphere(center.position + Vector3.forward * rangeOuter, 5.0f);

        Gizmos.DrawLine(center.position, center.position + Vector3.right * rangeOuter);
        Gizmos.DrawWireSphere(center.position + Vector3.right * rangeOuter, 5.0f);

        Gizmos.DrawLine(center.position, center.position + Vector3.back * rangeOuter);
        Gizmos.DrawWireSphere(center.position + Vector3.back * rangeOuter, 5.0f);

        Gizmos.DrawLine(center.position, center.position + Vector3.left * rangeOuter);
        Gizmos.DrawWireSphere(center.position + Vector3.left * rangeOuter, 5.0f);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(center.position + Vector3.forward * rangeInner, 5.0f);
        Gizmos.DrawWireSphere(center.position + Vector3.right * rangeInner, 5.0f);
        Gizmos.DrawWireSphere(center.position + Vector3.back * rangeInner, 5.0f);
        Gizmos.DrawWireSphere(center.position + Vector3.left * rangeInner, 5.0f);
    }

    private void OnDestroy()
    {
        EnemyBase.OnKilled -= OnEnemyKilled;
    }
}