using Unity.AI.Navigation;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }

    [Header("Player Spawn Details")]
    [SerializeField] private Player playerPrefab;
    [SerializeField] private Transform playerSpawnLocation;

    [Header("Navigation")]
    [SerializeField] private NavMeshSurface levelNavMesh;
    public NavMeshSurface NavMeshSurface => NavMeshSurface;

    [Header("Waves")]
    [SerializeField] private WaveManager waveManager;

    public Player Player { get; private set; }

    private void Awake()
    {
        // Singleton
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        StartGame();
    }

    private void StartGame()
    {
        Player = Instantiate(playerPrefab, playerSpawnLocation);
        waveManager.StartWaves();
    }
}
