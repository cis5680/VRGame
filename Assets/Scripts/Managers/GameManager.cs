using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public int score { get; private set; }
    public int highScore { get; private set; }
    public float health { get; private set; }

	[SerializeField] private float PLAYER_INITIAL_HEALTH;

    private SpellbookInterface spellbook;

    [SerializeField] private bool testInPCMode = false;
    public bool TestInPCMode => testInPCMode;

    private void Awake()
    {
        // If there is no Instance, make myself the instance
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            // Else if there is already an instance and it is not me, delete myself

            Destroy(this);  // Destroy only *this* specific component, not the whole GameObject 
                            // Moved DontDestroyOnLoad to a specific script
                            // I'll explain the reason for this when we're together
        }


        // Initial Player Stats
        highScore = 0;

        SceneManager.sceneLoaded += OnSceneLoaded;
	}

    private void Start()
    {
		resetGame();
	}

	void OnSceneLoaded(Scene scene, LoadSceneMode mode)
	{
        if (scene.name == "BasicScene")
        {
			resetGame();
        }
	}

	// HEALTH
	public void updateHealth(float delta)
    {
        health += delta;
        spellbook.UpdateHealthUI(health / PLAYER_INITIAL_HEALTH);
    }
    public float getHealth()
    {
        return health;
    }

    // SCORE
    public void updateScore(int delta)
    {
        score += delta;
    }

    public float getScore()
    {
        return score;
    }

    // WIN / LOSS CONDITIONS
    public bool checkWinLoss()
    {
        // return true if end game state reached, false to continue playing
        if (health <= 0.0f)
        {
            // LOSS

            SceneManager.LoadScene("GameOverScene");
            return true;
        }
        return false;
    }

    public bool checkHighScore()
    {
        if (score > highScore)
        {
            highScore = score;
            return true;
        }
        return false;
    }

    public void resetGame()
    {
        score = 0;
        health = PLAYER_INITIAL_HEALTH;
		spellbook = GameObject.FindObjectOfType<SpellbookInterface>(true);
	}

	private void OnDestroy()
    {
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}
}
