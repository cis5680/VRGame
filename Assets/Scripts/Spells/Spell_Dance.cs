using System;

public class Spell_Dance : SpellBase
{
	public static bool IsDancing { get; private set; }

	public static Action OnStartDancing;
	public static Action OnStopDancing;
	protected override void CastSpell() {
		IsDancing = !IsDancing;
		if (IsDancing)
        {
			OnStartDancing?.Invoke();
        }
		else
        {
			OnStopDancing?.Invoke();
        }
	}
}
