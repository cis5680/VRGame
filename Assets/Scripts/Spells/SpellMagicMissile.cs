using UnityEngine;
public class SpellMagicMissile : SpellBase
{
	[SerializeField] private MagicMissile missilePrefab;
	[HideInInspector] public Transform missileSpawnLocation;

	protected override void CastSpell()
	{
        Debug.Log("Derived Magic Spell cast");
        MagicMissile missile = Instantiate(missilePrefab, missileSpawnLocation.position, missileSpawnLocation.rotation);
		missile.Shoot(missileSpawnLocation.forward);
	}
}
