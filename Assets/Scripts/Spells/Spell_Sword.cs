using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell_Sword :SpellBase
{
	[SerializeField] private PowerSword powerSword;
	[HideInInspector] public Transform swordSpawnLocation;

	private Quaternion originalRot;

    protected override void Awake()
    {
        base.Awake();
		originalRot = powerSword.transform.rotation;
		powerSword.Deactivate();
    }

    protected override void CastSpell()
	{
		powerSword.transform.position = swordSpawnLocation.position;
		powerSword.transform.rotation = originalRot;

		powerSword.Activate();
	}
}