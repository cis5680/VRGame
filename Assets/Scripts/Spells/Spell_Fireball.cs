using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell_Fireball :SpellBase
{
	[SerializeField] private Fireball fireballPrefab;
	[HideInInspector] public Transform fireballSpawnLocation;

	protected override void CastSpell() {
		Debug.Log("Derived Fireball Spell cast");
		Fireball fireball = Instantiate(fireballPrefab, fireballSpawnLocation.position, fireballSpawnLocation.rotation);
		fireball.Shoot(fireballSpawnLocation.forward);
	}
}