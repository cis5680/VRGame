using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PowerBeam : MonoBehaviour, IPlayerWeapon
{
    [SerializeField] private float shootForce;
    [SerializeField] private float damage;
	[SerializeField] private AudioClip slashSfx;

	private Rigidbody rb;

    public float DamageToEnemy => damage;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void Setup(float damage)
    {

    }

    public void Shoot(Vector3 dir)
    {
        rb.AddForce(dir * shootForce, ForceMode.Impulse);
    }

    public bool CanDamageEnemy()
    {
        return true;
    }

    public void OnEnemyHit(EnemyBase enemy)
    {
    }

    void Start()
    {
		AudioSource.PlayClipAtPoint(slashSfx, transform.position);
	}
}
