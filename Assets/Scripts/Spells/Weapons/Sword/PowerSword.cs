using System.Collections;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

[RequireComponent(typeof(XRGrabInteractable), typeof(Rigidbody))]
public class PowerSword : MonoBehaviour, IPlayerWeapon
{
    private Rigidbody rb;

    [SerializeField] private float beamCooldown = 0.5f;
    [SerializeField] private float hitDamage = 1.0f;
    public float DamageToEnemy => hitDamage;

    private Camera playerCam;

    [SerializeField] private PowerBeam beamPrefab;
    [SerializeField] private Transform beamSpawnPos;

    private XRGrabInteractable grabInteractable;
    private XRBaseController controllerHoldingMe = null;

    private Plane plane;

    [Header("Shooting")]
    [SerializeField] private float shootSpeedThreshold = 0.5f;
    private float timeSinceTrackingTrajectory = 0.0f;
    private float trajectoryTrackProgress = 0.0f;
    [SerializeField] private float minShootDist = 4.0f;
    [SerializeField] private float trajectoryTrackDuration = 0.7f;

    private Vector3 lastMove;
    private Vector3 moveDelta;
    private float moveSpeed;
    private Vector3 startPt, endPt, startForward, endForward;
    private Vector3 trajectoryLine;
    private bool isPlayerHoldingMe = false;
    private bool canShootBeam = true;
    private bool isTrackingTrajectory = false;

    [Header("Haptics")]
    [SerializeField] private float hapticIntensity = 0.1f;
    [SerializeField] private float hapticDuration = 0.1f;
    [SerializeField] private AnimationCurve slashHapticCurve;
    [SerializeField] private AnimationCurve shootHapticCurve;
    [SerializeField] private float shootHapticDuration = 0.3f;
    private Coroutine shootBeamHapticCoroutine = null;

    [Header("Visibility Animations")]
    [SerializeField] private SwordDissolveHandler swordDissolveHandler;

    [Header("Destroy Sword")]
    [SerializeField] private float destroyInTime = 10.0f;

    [SerializeField] private bool drawDebugs = false;

    private void Awake()
    {
        grabInteractable = GetComponent<XRGrabInteractable>();
        playerCam = Camera.main;
    }

    // Start is called before the first frame update
    void Start()
    {
        lastMove = Vector3.zero;
        moveDelta = Vector3.zero;

        isPlayerHoldingMe = GameManager.Instance.TestInPCMode;  // debug will make this true always
        plane = new Plane(Vector3.forward, new Vector3(playerCam.transform.position.x, playerCam.transform.position.y, playerCam.transform.position.z + 0.5f));
    }

    [ContextMenu("Activate")]
    public void Activate()
    {
        if (rb == null)
        {
            rb = GetComponent<Rigidbody>();
        }

        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.useGravity = false;
        gameObject.SetActive(true);
        swordDissolveHandler.ToggleVisibility(true);

        Invoke(nameof(BeginDisappearing), destroyInTime);
    }

    private void BeginDisappearing()
    {
        swordDissolveHandler.ToggleVisibility(false, Deactivate);
    }

    [ContextMenu("Deactivate")]
    public void Deactivate()
    {
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        grabInteractable.selectEntered.AddListener(OnGrabbed);
        grabInteractable.lastSelectExited.AddListener(OnDroppedOrThrown);
    }

    private void OnGrabbed(SelectEnterEventArgs args)
    {
        if (args.interactorObject is XRBaseControllerInteractor controllerInteractor)
        {
            controllerHoldingMe = controllerInteractor.xrController;
            isPlayerHoldingMe = true;
        }
    }

    private void OnDroppedOrThrown(SelectExitEventArgs args)
    {
        isPlayerHoldingMe = false;
        controllerHoldingMe = null;
        if (shootBeamHapticCoroutine != null)
        {
            StopCoroutine(shootBeamHapticCoroutine);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!isPlayerHoldingMe)
        {
            return;
        }

        if (GameManager.Instance.TestInPCMode)
        {
            Ray ray = playerCam.ScreenPointToRay(Input.mousePosition);

            if (Input.GetMouseButton(0))
            {

                if (plane.Raycast(ray, out float t))
                {
                    Vector3 hitPt = ray.GetPoint(t);

                    transform.position = hitPt;
                }
			}

            transform.rotation = Quaternion.LookRotation(transform.position - playerCam.transform.position);
            //transform.LookAt(playerCam.transform);
        }

		moveDelta = beamSpawnPos.position - lastMove;
        moveSpeed = moveDelta.sqrMagnitude;
        //Debug.Log(moveSpeed);

        lastMove = beamSpawnPos.position;

        if (!isTrackingTrajectory && canShootBeam && moveSpeed > shootSpeedThreshold)
        {
            isTrackingTrajectory = true;
            startPt = beamSpawnPos.position;
            startForward = beamSpawnPos.forward;
        }

        if (isTrackingTrajectory)
        {
            if (timeSinceTrackingTrajectory < 0.0f)
            {
                timeSinceTrackingTrajectory = 0.0f;
                trajectoryTrackProgress = 0.0f;
            }
            else
            {
                timeSinceTrackingTrajectory += Time.deltaTime;
                trajectoryTrackProgress = timeSinceTrackingTrajectory / trajectoryTrackDuration;

                // continuous vibrations
                // the "0.333f" comes from here: https://forum.unity.com/threads/haptic-feedback-in-xr.1011787/#post-9484279
                // I had the time initially set to Time.fixedDeltaTime and I felt the stutter mentioned in the forum
                controllerHoldingMe.SendHapticImpulse(slashHapticCurve.Evaluate(trajectoryTrackProgress), 0.1f);

                if (timeSinceTrackingTrajectory >= trajectoryTrackDuration)
                {
                    endPt = beamSpawnPos.position;
                    endForward = beamSpawnPos.forward;

                    CheckAndShootBeam();
                }
            }
        }
        else if (timeSinceTrackingTrajectory >= 0.0f)
        {
            timeSinceTrackingTrajectory = -1.0f;
        }
    }

    private void CheckAndShootBeam()
    {
        isTrackingTrajectory = false;

        trajectoryLine = endPt - startPt;
        if (Vector3.SqrMagnitude(trajectoryLine) >= (minShootDist * minShootDist))
        {
            ShootBeam();
        }
    }

    private void ShootBeam()
    {
        if (canShootBeam)
        {
            //Debug.Log("Shooting beam");
            canShootBeam = false;
            Invoke(nameof(ResetBeamCooldown), beamCooldown);

            if (drawDebugs)
            {
                Debug.DrawLine(startPt, endPt, Color.green, 1.0f);
            }

            Vector3 spawnPt = (endPt + startPt) * 0.5f;
            Vector3 forwardDir = (spawnPt - playerCam.transform.position).normalized;

            //forwardDir = Vector3.ProjectOnPlane(forwardDir, Vector3.up);

            Quaternion rot = Quaternion.FromToRotation(Vector3.right, trajectoryLine.normalized);
            PowerBeam beam = Instantiate(beamPrefab, spawnPt, rot);
            beam.Shoot(forwardDir);

            if (shootBeamHapticCoroutine != null)
            {
                StopCoroutine(shootBeamHapticCoroutine);
            }
            shootBeamHapticCoroutine = StartCoroutine(ShootHaptic());
        }
    }

    private IEnumerator ShootHaptic()
    {
        float timeSinceStarted = 0.0f;
        float percentageComplete = 0.0f;

        while (percentageComplete <= 1.0f)
        {
            timeSinceStarted += Time.deltaTime;
            percentageComplete = timeSinceStarted / shootHapticDuration;

            controllerHoldingMe.SendHapticImpulse(shootHapticCurve.Evaluate(percentageComplete), 0.1f);

            yield return null;
        }
    }

    private void ResetBeamCooldown()
    {
        canShootBeam = true;
    }

    public bool CanDamageEnemy()
    {
        return isPlayerHoldingMe;
    }

    public void OnEnemyHit(EnemyBase enemy)
    {
        controllerHoldingMe.SendHapticImpulse(hapticIntensity, hapticDuration);
    }

    private void OnDisable()
    {
        grabInteractable.selectEntered.RemoveListener(OnGrabbed);
        grabInteractable.lastSelectExited.RemoveListener(OnDroppedOrThrown);
    }
}
