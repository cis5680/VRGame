using System;
using System.Collections;
using UnityEngine;

[ExecuteInEditMode]
public class SwordDissolveHandler : MonoBehaviour
{
    [SerializeField] MeshRenderer meshRenderer;
    [SerializeField] MeshRenderer glowRenderer;
    [SerializeField] private Transform dissolveHandler, beginTarget, endTarget;

    [SerializeField] private float activateDuration, deactivateDuration;
    [SerializeField] private AnimationCurve activateCurve, deactivateCurve;

    private Coroutine visibilityCoroutine = null;

    // Update is called once per frame
    void Update()
    {
        //meshRenderer.sharedMaterial.SetFloat("_DissolveHeight", dissolveHandler.localPosition.y);
        //glowRenderer.sharedMaterial.SetFloat("_DissolveHeight", dissolveHandler.localPosition.y);
    }

    public void ToggleVisibility(bool on, Action onDone = null)
    {
        Transform target = on ? endTarget : beginTarget;
        float duration = on ? activateDuration : deactivateDuration;
        AnimationCurve curve = on ? activateCurve : deactivateCurve;

        if (visibilityCoroutine != null)
        {
            StopCoroutine(visibilityCoroutine);
        }
        visibilityCoroutine = StartCoroutine(MoveHandle(target, duration, curve, onDone));
    }

    private IEnumerator MoveHandle(Transform target, float duration, AnimationCurve curve, Action onDone = null)
    {
        float timeSinceStarted = 0.0f;
        float percentageComplete = 0.0f;

        Vector3 startPos = dissolveHandler.transform.position;
        Vector3 endPos = target.position;

        while (timeSinceStarted <= duration)
        {
            timeSinceStarted += Time.deltaTime;
            percentageComplete = timeSinceStarted / duration;

            dissolveHandler.position = Vector3.LerpUnclamped(startPos, endPos, curve.Evaluate(percentageComplete));

            meshRenderer.material.SetFloat("_DissolveHeight", dissolveHandler.localPosition.y);
            glowRenderer.material.SetFloat("_DissolveHeight", dissolveHandler.localPosition.y);

            yield return null;
        }

        dissolveHandler.position = endPos;
        meshRenderer.material.SetFloat("_DissolveHeight", dissolveHandler.localPosition.y);
        glowRenderer.material.SetFloat("_DissolveHeight", dissolveHandler.localPosition.y);

        onDone?.Invoke();
    }    
}
