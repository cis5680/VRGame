﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MagicMissile : MonoBehaviour, IPlayerWeapon
{
    [SerializeField] private float damage = 1.0f;
    public float DamageToEnemy => damage;

    [SerializeField] private float shootForce = 10.0f;

    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public bool CanDamageEnemy()
    {
        return true;
    }

    public void OnEnemyHit(EnemyBase enemy)
    {
        // return;
    }

    public void Shoot(Vector3 direction)
    {
        rb.AddForce(direction * shootForce, ForceMode.Impulse);
    }
}
