using UnityEngine;

public class Fireball : MonoBehaviour
{
	[SerializeField] private Fireball_Blast AOE;

	[SerializeField] private float shootForce = 10.0f;
	[SerializeField] private float aoe_radius = 10.0f;
	[SerializeField] private float activateInTime = 0.1f;
	
	private Rigidbody rb;
	private bool canTrigger = false;
	private bool hasTriggered = false;

	private void Awake() {
		rb = GetComponent<Rigidbody>();
	}

	private void Start()
	{
		Invoke(nameof(Activate), activateInTime);
	}

	private void Activate()
	{
		canTrigger = true;
	}

	private void OnTriggerEnter(Collider other) {
		if (hasTriggered || !canTrigger)
        {
			return;
        }

		hasTriggered = true;
		Fireball_Blast blast = Instantiate(AOE, transform.position, Quaternion.identity);
		blast.transform.localScale = new Vector3(aoe_radius, aoe_radius, aoe_radius);
		blast.Trigger();

		Destroy(gameObject);
	}

	public void Shoot(Vector3 direction) {
		rb.AddForce(direction * shootForce, ForceMode.Impulse);
	}
}
