using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

[RequireComponent(typeof(XRGrabInteractable))]
public class WandTrail : MonoBehaviour
{
	private bool isPlayerHoldingMe = false;
	private Camera playerCam;
	private XRGrabInteractable grabInteractable;
	[SerializeField] private TrailRenderer trailRenderer;
	[SerializeField] private ParticleSystem trailParticles;

	Plane plane;

	void Start()
    {
		grabInteractable = GetComponent<XRGrabInteractable>();
		playerCam = Camera.main;
		trailParticles.gameObject.SetActive(false);
		isPlayerHoldingMe = GameManager.Instance.TestInPCMode;
		plane = new Plane(Vector3.forward, new Vector3(playerCam.transform.position.x, playerCam.transform.position.y, playerCam.transform.position.z + 0.5f));
	}

    void FixedUpdate()
    {
		if (GameManager.Instance.TestInPCMode)
		{
			Ray ray = playerCam.ScreenPointToRay(Input.mousePosition);
			if (plane.Raycast(ray, out float t))
			{
				Vector3 hitPt = ray.GetPoint(t);
				transform.position = hitPt;
			}
			transform.LookAt(playerCam.transform);
			trailRenderer.time = Input.GetMouseButton(0) ? 5.0f : 0.3f;
			trailParticles.gameObject.SetActive(Input.GetMouseButton(0));
		}
		else
		{
			float castValue = Input.GetAxis("RightControllerTrigger");
			bool castSpellButtonHeld = castValue > 0.1;
			trailRenderer.time = castSpellButtonHeld ? 5.0f : 0.3f;
			trailParticles.gameObject.SetActive(castSpellButtonHeld);
		}
	}

}
