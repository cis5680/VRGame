using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball_Blast : MonoBehaviour, IPlayerWeapon
{
	[SerializeField] private float damage = 1.0f;
	[SerializeField] private float duration = 2.0f;
	public float DamageToEnemy => damage;

	private Collider my_col;

	// Start is called before the first frame update
	void Awake() {
		my_col = GetComponent<Collider>();
	}


	public bool CanDamageEnemy() {
		return true;
	}

	public void OnEnemyHit(EnemyBase enemy) {
		// welp
	}

	public void Trigger() {
		my_col.enabled = true;
		StartCoroutine(DestroySelf());
	}

	private IEnumerator DestroySelf() {
		yield return new WaitForSeconds(duration);
		Destroy(gameObject);
	}
}
