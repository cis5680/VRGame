using UnityEngine;

public abstract class SpellBase : MonoBehaviour
{
	public SpellData spellData;

	public bool isOnCooldown { get; private set; }
	public float cooldownTimeRemaining { get; private set; }

	protected virtual void Awake()
    {

    }

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isOnCooldown) {
			cooldownTimeRemaining -= Time.deltaTime;
			if (cooldownTimeRemaining <= 0) {
				isOnCooldown = false;
			}
		}
    }

	public bool TryCastSpell() {
		if (isOnCooldown)
		{
			return false;
		}

		isOnCooldown = true;
		cooldownTimeRemaining = spellData.cooldownDuration;

		if (spellData.castSfx != null)
        {
			AudioSource.PlayClipAtPoint(spellData.castSfx, transform.position);
		}
		CastSpell();
		return true;
    }

	protected abstract void CastSpell();
}
