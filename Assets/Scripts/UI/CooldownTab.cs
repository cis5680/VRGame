using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CooldownTab : MonoBehaviour
{
	[SerializeField] private Image icon;
	[SerializeField] private Image fill;
	[SerializeField] private Color color;

	public void SetFill(float _percent) {
		fill.fillAmount = _percent;
	}

	public void SetIcon(Sprite _icon) {
		icon.sprite = _icon;
	}

	public void SetColor(Color _color) {
		color = _color;
	}

	public void Copy(CooldownTab _tab) {
		icon.sprite = _tab.icon.sprite;
		fill.fillAmount = _tab.fill.fillAmount;
		color = _tab.color;
		foreach (Image image in GetComponentsInChildren<Image>()) {
			image.color = color;
		}
	}

	public void Deactivate() {
		foreach (Image image in GetComponentsInChildren<Image>()) {
			image.color = Color.clear;
		}
	}

	public void Activate() {
		foreach (Image image in GetComponentsInChildren<Image>()) {
			image.color = color;
		}
	}
}
