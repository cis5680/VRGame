public interface IDamageable
{
    public void TakeDamage(float damage);
}

public interface IHealable
{
    void Heal(float healAmount);
}

public interface IPlayerWeapon
{
    public float DamageToEnemy { get; }

    bool CanDamageEnemy();
    void OnEnemyHit(EnemyBase enemy);
}
