using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Spell", menuName = "Spells / New Spell")]
public class SpellData : ScriptableObject
{
	[Range(0f, 120f)]
	public float cooldownDuration;

	public Sprite icon;
	
	public Color color;
	
	public Texture2D glyph;

	public GestureName inputIdentifier;

	public AudioClip castSfx;
}
