﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Horde Survival Wave", menuName = "Waves/Horde Survival Wave")]
public class HordeSurvivalWaveData : WaveData
{
    [System.Serializable]
    public struct EnemySpawnDetails
    {
        [SerializeField] private EnemyType enemyType;
        public EnemyType EnemyType => enemyType;

        [SerializeField, Range(0.0f, 1.0f)] private float spawnChance;
        public float SpawnChance => spawnChance;
    }

    public override WaveType WaveType => WaveType.HordeSurvival;

    // Number of enemies to spawn in this wave
    [SerializeField] private int numEnemies;
    public int NumEnemies => numEnemies;

    [SerializeField] private EnemySpawnDetails[] enemyDetails;
    public EnemySpawnDetails[] EnemyDetails => enemyDetails;

    [SerializeField] private int subWaveSpawnCount;
    public int SubWaveSpawnCount => subWaveSpawnCount;

    [SerializeField] private float waitBetweenSubWaves;
    public float WaitBetweenSubWaves => waitBetweenSubWaves;
}
