﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Wait Wave", menuName = "Waves/Wait Wave")]
public class WaitWaveData : WaveData
{
    [SerializeField] private float waitTime;
    public float WaitTime => waitTime;

    public override WaveType WaveType => WaveType.Wait;
}