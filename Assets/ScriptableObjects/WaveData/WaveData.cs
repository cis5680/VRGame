﻿using UnityEngine;

// This script defines data holders for all possible types of waves
public enum WaveType
{
    HordeSurvival,
    Wait
}

public abstract class WaveData : ScriptableObject
{
    public abstract WaveType WaveType { get; }
}
