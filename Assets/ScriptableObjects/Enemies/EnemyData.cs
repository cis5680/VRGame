﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "Enemies / New Enemy")]
public class EnemyData : ScriptableObject
{
    [SerializeField] private EnemyType enemyType;
    public EnemyType EnemyType => enemyType;

    [SerializeField] private EnemyBase enemyPrefab;
    public EnemyBase EnemyPrefab => enemyPrefab;
}
