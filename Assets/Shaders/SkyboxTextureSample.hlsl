#ifndef SKYBOXTEXTURESAMPLE
#define SKYBOXTEXTURESAMPLE

void CoordinateSystem(in float3 v1, out float3 v2, out float3 v3) {
    if (abs(v1.x) > abs(v1.y))
            v2 = float3(-v1.z, 0, v1.x) / sqrt(v1.x * v1.x + v1.z * v1.z);
        else
            v2 = float3(0, v1.z, -v1.y) / sqrt(v1.y * v1.y + v1.z * v1.z);
        v3 = cross(v1, v2);
}

float3x3 WorldToLocal(float3 nor) {
    float3 tan, bit;
    bit = float3(0,1,0);
    tan = cross(bit, nor);
    bit = cross(nor, tan);
    // CoordinateSystem(nor, tan, bit);
    return float3x3(tan, bit, nor);
}

float3x3 LocalToWorld(float3 nor) {
    return transpose(WorldToLocal(nor));
}

void GetSunDirViewSpace_float(float3 ViewDir, out float3 SunDirViewSpace)
{
    SunDirViewSpace = mul(WorldToLocal(ViewDir), _SunDirection);
}

void GetViewDirSunSpace_float(float3 ViewDir, out float3 ViewDirSunSpace)
{
    ViewDirSunSpace = mul(WorldToLocal(_SunDirection), ViewDir); 
}

void DirectionToColor_float(float3 Direction, out float3 Color)
{
    Color = Direction * 0.5f + float3(0.5f, 0.5f, 0.5f);
}

void DotSunViewInSunSpace_float(float3 ViewDir_W, out float dotVal)
{
    float3 ViewDir_S;
    GetViewDirSunSpace_float(ViewDir_W, ViewDir_S);

    dotVal = ViewDir_S.z;
}

#endif